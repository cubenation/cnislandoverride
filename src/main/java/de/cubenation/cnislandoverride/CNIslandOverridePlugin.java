package de.cubenation.cnislandoverride;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by BenediktHr on 26.03.16.
 * Project: CNIslandOverride
 */
public class CNIslandOverridePlugin extends JavaPlugin implements Listener {

    private String prefix = ChatColor.GRAY + "[" + ChatColor.GREEN + "Cube-Nation" + ChatColor.GRAY + "]" + ChatColor.RESET;

    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onPlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        String msg = event.getMessage();

        if (msg.equalsIgnoreCase("/is") || msg.equalsIgnoreCase("island") || msg.equalsIgnoreCase("ai")) {
            if (!player.hasPermission("cnisland.verified")) {

                player.sendMessage(prefix + " " + ChatColor.RED + "Du bist noch nicht freigeschaltet! " + ChatColor.WHITE + "Lies dafür einfach die Schilder am Spawn.");
                event.setCancelled(true);
            }
        }
    }

}
